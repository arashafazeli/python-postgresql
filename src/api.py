from fastapi import FastAPI

import psycopg

app = FastAPI()


@app.on_event("startup")
def startup():
    app.db = psycopg.connect("postgresql://postgres:testpass@localhost/postgres")

@app.on_event("shutdown")
def shutdown():
    app.db.close()

@app.get("/stores")
def stores():
    with app.db.cursor() as cur:
        cur.execute("SELECT name FROM stores")
        names = cur.fetchall()
        names = [name[0] for name in names]
        return names
